import './App.css';
import FetchHandler from './Components/FetchHandler';

function App() {
  return (
    <div className="App">
     <FetchHandler />
    </div>
  );
}

export default App;
